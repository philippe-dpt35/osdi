# OSDI #
### **O**util de **S**auvegarde et de **D**épannage **I**nformatique ###

OSDI est un live CD/USB disposant de nombreux outils destinés au dépannage, la sauvegarde-restauration et la récupération de données.
Il ouvre automatiquement une session root sans mot de passe.

La distribution est proposée en 2 versions basées sur Ubuntu 16.04 (Xenial) ou 18.04 (Bionic), et deux architectures, 32 et 64 bits.

Compatibilité:

|      **Version**      | **Legacy** | **UEFI** | **secure boot** |
|:-----------------:|:------:|:----:|:-----------:|
|  OSDI-i386-Xenial |   oui  |  oui |     oui     |
|  OSDI-i386-Bionic |   oui  |  oui |     non     |
| OSDI-amd64-Bionic |   oui  |  oui |     oui     |

Vous pouvez télécharger une image iso, ou construire vous-même votre propre image personnalisée grâce aux scripts et aux sources fournies en les modifiant selon vos besoins.

## Contenu de la distribution ##

### Pour le dépannage ###

- Afficheur d'informations système **lshw**
- Afficheur d'informations système et outil de benchmark **hardinfo**
- Réparateur de dépannage **boot-repair**
- Deux scripts permettant de passer en chroot dans un répertoire de son choix en montant et démontant automatiquement les répertoires système nécessaires : **lance-chroot** et **quitte-chroot**
- Vérificateur d'état des disques durs **gsmartcontrol**

### Pour la sauvegarde ###
- **Clonezilla**
- **Redo backup & Recovery**

### Pour la récupération de données ###

- Récupérateur de données en ligne de commande **photorec**
- **qphotorec** pour utiliser photorec en mode graphique
- récupérateur de données en ligne de commande **foremost**

### Outils divers ###

- Éditeur de partitions **gparted**
- Gestionnaire de volumes logiques LVM **system-config-lvm**
- Outil de configuration NTFS **ntfs-config-root**
- Utilitaire de disques **gnome-disk-utility**
- Visionneuse d'images **gpicview**
- Archiveur **Xarchiver**
- Lecteur de fichiers pdf **xpdf**
- Analyseur d'usage disque **Baobab**
- Réinitialisation de disques à l'état d'usine **drivereset**
- Outil de création de disques de démarrage **usb-creator-gtk**
- Gestionnaire de réseau **network-manager-gnome**
- Navigateur Web **Midori** sur la version Xenial, Epiphany pour les versions Bionic, choisis pour leur légèreté.
- gestionnaire de fichiers **PCManFM**
- Mixeur audio **Alsamixergui**


## Utilisation des scripts ##

La construction se faisant avec des droits root, il est recommandé de le faire dans une machine virtuelle. Les paquets xorriso, debootstrap et squashfs-tools doivent avoir été installés. 

Les scripts ont été testés sur des Ubuntu ou dérivées 16.04 et 18.04.

Téléchargez les deux scripts `operations-chroot.sh` et `construit-osdi.sh`, ainsi que les dossiers `includes.chroot`, `deb` et `build-iso` dans un dossier de votre choix. Ouvrez un terminal et lancez :

Pour la version Xenial :

    sudo ./construit-osdi.sh

Pour la version Bionic :

    sudo ./construit-osdi.sh i386
    
ou

    sudo ./construit-osdi.sh amd64
    
selon l'architecture souhaitée.

Le script construit une image osdi-i386-xxx.iso ou osdi-amd64.xxx.iso dans le répertoire de l'utilisateur courant (xxx correspondant à Xenial ou Bionic)..

## Personnalisations ##

### Logiciels ###

L'ajout ou le retrait de paquets se fait dans les variables :

- `paquets` qui contient les paquets des dépôts officiels;
- `paquets_complementaires` pour des paquets hors dépôts officiels; il faudra alors ajouter leur dépôt dans le script;
- `paquets_locaux` pour des paquets personnels que l'on devra placer dans le dossier `deb`.

### Paramétrages ###

Les paramétrages sont à faire dans le répertoire `includes.chroot`, dans les dossiers correspondant à ceux d'un système opérationnel.

Pour les fichiers de configuration de l'utilisateur courant, ceux-ci sont à placer dans le répertoire `root`, qui est l'utilisateur courant du live CD/USB.

## Informations complémentaires ##

Les dossiers `build-iso`, contenant les fichiers nécessaires à la construction de l'iso, ont été obtenus par hack des dossiers d'iso Ubuntu desktop.

Le fichier `isohdpfx.bin`,contenant le mbr de l'iso, peut-être obtenu de deux façons : 

- installation du paquet isolinux sur une Ubuntu de version identique à celle construite, et récupération depuis le dossier `/usr/lib/ISOLINUX` ;

- en le construisant depuis une iso Ubuntu de version identique à celle construite avec la commande suivante (exemple donné pour la version 18.04 64 bits)

    sudo dd if=ubuntu-18.04.2-desktop-amd64.iso bs=512 count=1 of=isohdpfx.bin

Les dossiers au sein de includes-chroot contiennent les fichiers de personnalisation de la distribution. Ils sont  obtenus par installation d'une distribution identique à celle que l'on veut construire, en la personnalisant et en récupérant les fichiers adéquats.

Y sont adjoints certains fichiers de paramétrages qui se sont avérés nécessaires au bon fonctionnement de la distribution. Ceux-ci ont été obtenus par résolution successive des problèmes lors des tests de construction.
