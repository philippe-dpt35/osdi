#! /bin/bash

# Prérequis: avoir installé les paquets squashfs-tools xorriso debootstrap et mis à jour les paquets

iso=~/iso
filesys=~/squashfs
# Redirection d'erreurs vers un fichier log
fichierlog="/var/log/construit-osdi.log"
date > "$fichierlog"
exec 2>>"$fichierlog"
err_chroot="$filesys/root/operations-chroot.log"
fic_liste_manquants="$filesys/root/liste-paquets-manquants.log"

# Vérification de la présence des outils nécessaires
if ! dpkg -l | grep 'xorriso' || ! dpkg -l | grep 'squashfs-tools' || ! dpkg -l | grep 'debootstrap'
  then echo "Les paquets nécessaires à la construction n'ont pas tous été installés.
Veuillez vérifier la présence des paquets xorriso, debootstrap et squashfs-tools,
puis relancez le script."
       exit 1
fi
## Préparation de l'environnement de travail
mkdir "$filesys"
mkdir "$iso"
# Dossier de création de l'iso
if ! [ -d "build-iso" ]
  then echo "Répertoire build-iso manquant."
       echo "Le script va s'arrêter."
       exit 1;
fi
rsync -av build-iso/. "$iso"
if ! [ -d "$iso/casper" ]
  then mkdir "$iso/casper"
fi
# Création d'un système minimal dans le dossier de construction
debootstrap --arch=i386 xenial "$filesys" http://archive.ubuntu.com/ubuntu/

cp /run/systemd/resolve/stub-resolv.conf "$filesys/run/resolvconf/resolv.conf"
# Copie dans le nouveau filesystem des scripts et dossiers nécessaires aux opérations
cp operations-chroot.sh "$filesys/tmp"
if ! [ -d "includes.chroot" ]
  then echo "Répertoire includes.chroot manquant."
       echo "Le script va s'arrêter."
       exit 1;
fi
cp -avr includes.chroot "$filesys/tmp"
if ! [ -d "deb" ]
  then echo "Répertoire des paquets locaux manquant."
       echo "Le script va s'arrêter."
       exit 1;
fi
cp -av deb "$filesys/tmp"
mount --bind /proc "$filesys/proc"

# On passe dans le nouveau système et on y lance les opérations
chroot "$filesys" /tmp/operations-chroot.sh

# On revient dans le système hôte et on récupère les noyaux système
umount "$filesys/proc"
# S'il y a plusieurs noyaux, on récupère la version la plus récente
cp "$filesys"/boot/$(ls "$filesys"/boot | grep 'vmlinuz-*' | sed '$!d') "$iso"/casper/vmlinuz
cp "$filesys"/boot/$(ls "$filesys"/boot | grep 'initrd.img-*' | sed '$!d') "$iso"/casper/initrd.img

# Récupère les log lors des opérations de chroot
echo "
=====================================
Erreurs lors des opérations de chroot
=====================================
" >>"$fichierlog"
cat "$err_chroot" >>"$fichierlog"
echo "
=====================================
" >>"$fichierlog"
cp "$fic_liste_manquants" "/tmp/liste-paquets-manquants.txt"

# Nettoyage des log et fichiers temporaires du filesystem
rm -rf  "$filesys"/tmp/*  "$filesys"/var/log/* "$filesys"/root/*.log

# On génère le nouveau filesystem
#chroot "$filesys" dpkg-query -W --showformat='${Package} ${Version}\n' > "$iso"/casper/filesystem.manifest
#chmod go-w "$iso"/casper/filesystem.manifest
cd "$filesys"
mksquashfs . "$iso"/casper/filesystem.squashfs
cd ~/
# Nettoyage du répertoire de construction
echo "Le répertoire de construction occupe un espace disque important.
Cela peut empêcher la création de l'iso sur une machine virtuelle si
l'espace disque alloué est insuffisant.
Espace disque libre sur la partition de travail : $(df -h ~/ | awk '/dev/ {print $4}')
Souhaitez-vous effacer le répertoire de travail ? (O/N défaut N)"
read choix
if [ "$choix" = "O" ] || [ "$choix" = "o" ]; then
  rm -rf "$filesys"
fi

# Construction de l'iso
echo "Construction de l'iso en cours."
xorriso -as mkisofs \
-isohybrid-mbr "$iso"/isolinux/isohdpfx.bin \
-c isolinux/boot.cat \
-b isolinux/isolinux.bin \
-no-emul-boot \
-boot-load-size 4 \
-boot-info-table \
-eltorito-alt-boot \
-e boot/grub/efi.img \
-no-emul-boot \
-isohybrid-gpt-basdat \
-o ~/OSDI-i386-Xenial.iso \
"$iso"

# Si des paquets n'ont pu être installés, on affiche leur nom
if [ -e "/tmp/liste-paquets-manquants.txt" ]; then
  echo "
++++++++++++++++++++++++++++++++++++++++++++++++
Les paquets suivants n'ont pas pu être installés
++++++++++++++++++++++++++++++++++++++++++++++++
"
cat "/tmp/liste-paquets-manquants.txt"
  else echo "Tous les paquets de la distribution ont pu être convenablement installés."
fi
echo "Un fichier log des opérations a été enregistré :
$fichierlog
"
rm "/tmp/liste-paquets-manquants.txt"

exit 0
