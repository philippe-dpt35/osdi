#! /bin/bash

# Indiquer ici le nombre de tentatives d'installation à faire automatiquement dansle cas où des installations ont échoué
tentatives=3

# Paquets à installer
paquets="linux-generic gksu rsync openssh-client libglib-perl libgtk2-perl libxml-simple-perl casper lupin-casper cifs-utils gvfs-backends dosfstools ntfs-3g ntfs-config hfsutils system-config-lvm wget dpkg time hdparm alsamixergui pm-utils libnotify-bin notify-osd notify-osd-icons beep wodim curlftpfs nmap libnotify-bin cryptsetup reiserfsprogs reiser4progs jfsutils python-software-properties software-properties-common lxdm lxde lxsession-logout xinit openbox desktop-base xserver-xorg x11-xserver-utils xserver-xorg-video-all lxrandr gtk-theme-switch shimmer-themes gnome-icon-theme gnome-brave-icon-theme dmz-cursor-theme maximus lxmenu-data ttf-ubuntu-font-family gtk2-engines-pixbuf libqtgui4 lxterminal leafpad gparted testdisk clonezilla xterm hardinfo baobab gpicview xpdf gsmartcontrol gnome-disk-utility smartmontools policykit-1-gnome policykit-desktop-privileges gsettings-desktop-schemas lshw-gtk usb-creator-gtk xarchiver network-manager-gnome foremost smbclient fsarchiver partclone midori"

# Paquets hors dépôt officiel
paquets_complementaires="boot-repair"

# Paquets locaux à installer
paquets_locaux="redo-fr.deb outils-chroot.deb qphotorec_1.0_all.deb"

# Pour établir la liste des paquets non installés
fic_liste_manquants="/root/liste-paquets-manquants.log"
liste_paquets=$(echo "$paquets" "$paquets_complementaires" | sed 's/ /\n/g')
liste_locaux=$(echo "$paquets_locaux" | sed 's/ /\n/g' | sed -e 's/.*\///' -e 's/\.*\.deb//' -e 's/_.*$//')

manquants=""
installes="/tmp/paquets-installes.txt"
erreurs="non"
etablit_manquants () {
  dpkg -l | sed -e '1,6d' -e "s/[ ][ ]*/#/g" | cut -d '#' -f 2 > "$installes"
  manquants=""
  erreurs="non"
  while read paquet
    do
    if ! grep "$paquet" "$installes" > /dev/null
      then manquants="$manquants $paquet"
          erreurs="oui"
    fi
  done <<<"$liste_paquets"
  manquants=$(echo "$manquants" | tr " " "\n" | sort -u | tr "\n" " " | sed 's/^ //')
}

# Redirection d'erreurs vers un fichier temporaire
fic_log="/root/operations-chroot.log"
> "$fic_log"
exec 2>>"$fic_log"

# Ajout des dépôts ubuntu xenial
echo "deb http://fr.archive.ubuntu.com/ubuntu/ xenial main restricted
deb http://fr.archive.ubuntu.com/ubuntu/ xenial-updates main restricted
deb http://fr.archive.ubuntu.com/ubuntu/ xenial universe
deb http://fr.archive.ubuntu.com/ubuntu/ xenial-updates universe
deb http://fr.archive.ubuntu.com/ubuntu/ xenial multiverse
deb http://fr.archive.ubuntu.com/ubuntu/ xenial-updates multiverse
deb http://fr.archive.ubuntu.com/ubuntu/ xenial-backports main restricted universe multiverse
deb http://security.ubuntu.com/ubuntu xenial-security main restricted
deb http://security.ubuntu.com/ubuntu xenial-security universe
deb http://security.ubuntu.com/ubuntu xenial-security multiverse
" >/etc/apt/sources.list

# Crée le compte root
echo -e "osdi\nosdi" | passwd root

# Reconfigure les locales
update-locale "LANG=fr_FR.UTF-8"
locale-gen --purge "fr_FR.UTF-8"
dpkg-reconfigure --frontend noninteractive locales

## Installation des paquets des dépôts officiels
apt-get update
apt-get dist-upgrade -y
for paquet in ${paquets}
do
  apt-get install -y --no-install-recommends "$paquet"
done

# Ajout des dépôts complémentaires
add-apt-repository --yes ppa:yannubuntu/boot-repair
apt-get update
# Installation des paquets hors dépôts officiels
for paquet in ${paquets_complementaires}
do
  apt-get install -y --no-install-recommends "$paquet"
done

# On tente la réinstallation des paquets dont l'installation a échoué
ct=1
while [ $ct -le $tentatives ]; do
  etablit_manquants
  if [ $erreurs = "oui" ]; then
    for paquet in ${manquants}; do
      apt-get install -y "$paquet"
    done
  fi
  let "ct += 1"
done
etablit_manquants
reponse="oui"
if [ "$erreurs" = "oui" ]; then
  while [ "$reponse" = "oui" ]; do
    echo ""
    echo "Les paquets suivants n'ont pas pu être installés malgré $tentatives tentatives :"
    echo "$manquants"
    echo "Voulez-vous faire une nouvelle tentative ? O/N (par defaut O)"
    read choix
    case "$choix" in
        [Oo]) reponse="oui" ;;
        [Nn]) reponse="non" ;;
        *) reponse="oui"
    esac
    if [ "$reponse" = "oui" ]; then
      for paquet in ${manquants}; do
         apt-get install -y "$paquet"
      done
      let "tentatives += 1"
    fi
    etablit_manquants
    if [ "$erreurs" = "non" ]; then
      break
    fi
  done
fi

# Paquets locaux à télécharger
wget -nv -P /tmp/deb https://sourceforge.net/projects/crunchyiconthem/files/QPhotoRec/qphotorec_1.0_all.deb

# Installe les paquets locaux
for paquet in ${paquets_locaux}; do
  yes | dpkg -i /tmp/deb/$paquet
done
apt-get install -f -y
rm -rf /tmp/deb

# Vérifie la bonne installation des paquets locaux
dpkg -l | sed -e '1,6d' -e "s/[ ][ ]*/#/g" | cut -d '#' -f 2 > "$installes"
while read paquet; do
  if ! grep "$paquet" "$installes" > /dev/null
      then manquants="$manquants $paquet"
           erreurs="oui"
    fi
done <<<"$liste_locaux"
# Si des paquets n'ont malgré tout pu être installés, on écrit lesquels dans le fichier log
if [ "$erreurs" = "oui" ]; then
  echo "
++++++++++++++++++++++++++++++++++++++++++++++++
Les paquets suivants n'ont pas pu être installés
++++++++++++++++++++++++++++++++++++++++++++++++
$manquants
" >> "$fic_log"
  echo "$manquants" >"$fic_liste_manquants"
fi

## Paramétrages de OSDI ##
rsync -av /tmp/includes.chroot/* /
rm -rf /tmp/includes.chroot

# Pour résoudre les problèmes de fonctionnement de partclone au sein de redo backup
rm /usr/share/locale/fr_FR/LC_MESSAGES/partclone.mo
ln /usr/bin/startlxde /usr/bin/startlubuntu

## Nettoyage ##
# Nettoyage avec Bleachbit
apt-get install -y bleachbit
bleachbit --clean system.*
apt-get purge -y bleachbit
rm -rf ~/.config/bleachbit
# Nettoyage des résidus de paquets
apt-get clean
apt-get autoclean
apt-get autoremove --purge -y
# Suppression des anciens noyaux
#dpkg -l linux-* | awk '/^ii/{ print $2}' | grep -v -e `uname -r | cut -f1,2 -d"-"` | grep -e [0-9] | grep -E '(image|headers)'  | xargs apt -y purge
# Supprime les fichiers devenus inutiles
rm "$installes"
rm $paquets_locaux
# Nettoyage des corbeilles
rm -rf /var/tmp/* ~/.local/share/Trash/* /root/.local/share/Trash/*
# Nettoyage de la distribution
rm -rf /usr/src/* /usr/include/* /usr/share/doc/* /usr/share/help/* /usr/share/man/*
# On génère les noyaux si nécessaire
if [ -z $(find /boot -name "vmlinuz*") ]
  then apt-get install --reinstall $(dpkg -l | grep 'linux-image-[0-9]' | tr -s ' ' | cut -d ' ' -f 2)
fi
update-initramfs -k all -u

exit 0

